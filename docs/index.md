# AMAP Les Adrets

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Récupérer la synthèse d'une permanence ?

Choisir "Mes livraisons" dans le menu.
![Mes livraisons](img/livraisons.png)

Cliquer sur le lien "Télécharger la feuille d'émargement hebdomadaire semaine X" au dessus du tableau de vos livraisons.

![Mes livraisons](img/emargement.png)

Le fichier contient les informations de livraisons de la semaine :

* cumuls de quantités par producteur
* quantités par amapien



## Ajouter un produit ?

Choisir "Gestion des produits" dans le sous-menu "Référents"

![Gestion des produits](img/produits.png)

Choisir un producteur dans la liste déroulante sous "Liste des produits"

Cliquer sur le bouton "Ajouter un produit"

Note : les prix des produits sont spécifiés au moment de la création de la commande. Voir "Comment créer une commande ?"



## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
